import java.util.HashMap;
import java.util.Arrays;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        /*Activity

            create a hasmap of the ff and their stock"
                - Mario Odyseey - 50
                - SSBU - 20
                Luigi's mansion 3 - 15
                pokemon sword - 30
                pokemon shield - 100
                note; integer istead of int in hasmap

            loop over the items in the hasmap obj and print out the content

            create an array lit called topgames.

            loop over each item in the games hashmap and check if the quantity of the current item being iterated is less than or equal to 30
                - if it is, the add the name of the game to our topGames array list and show a message.
              print the value of the topGames list in the terminal

              crate git repo named WFC043-s03


        */

        HashMap<String, Integer> games = new HashMap<>();

        ArrayList<String> topGames = new ArrayList<>();

        games.put("Mario Odyssey", 50);
        games.put("Super Smash Bros. Ultimate", 20);
        games.put("Luigi's Mansion 3", 15);
        games.put("Pokemon Sword", 30);
        games.put("Pokemon Shield", 100);

        games.forEach((key, value)->{
            System.out.println(key + " has " + value + " stocks left.");
            if(value <= 30){
                topGames.add(key);
                System.out.println(key + " has been added to top games list!");
            }
        });

        System.out.println("Our Shop's top games: " + topGames);





    }
}