import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Loops are control control structures that allow code blocks to be repeated based on conditions set.
        // Types of Loops: For loop, while loop, dowhile loop

        //While Loop
 /*       int a = 1;
        while(a < 5){
            System.out.println("While Loop Counter: " + a);
            a++;
        }
 */

        //do-while loop
/*
        int b = 12;
        do{
            System.out.println("Countdown: " + b);
            b--;
        }while(b > 10);
*/


        //while loop with user input
        Scanner appScanner = new Scanner(System.in);
        String name = "";

/*        while(name.isBlank()){
            System.out.println("What's your name?");
            name = appScanner.nextLine();

            if(!name.isBlank()){
                System.out.println("Hi! " + name);
            }
        }
 */
/*
        System.out.println(name.isBlank()); // will return trye, if there are no characters or if the length of the string is 0, and if the string contains a whitespace.
        System.out.println(name.isEmpty()); // will return false, if there is atleast whitespace in the string
*/
        //For loops

/*        for(int i = 1; i <= 10; i++){
            System.out.println("Count: " + i);
        }
 */
        // iterate over an array for loop
/*

        int[] intArray = {100, 200, 300, 400, 500};
        for(int i = 0; i < intArray.length; i++){
            System.out.println("Item Index " + intArray[i]);
        }
*/

        //Loop over multidimensional array
        //A two-dimensional array, which can be best described by two lengths nested within each other like a matrix.

        String[][] classroom = new String[3][3];
        //First Row
        classroom[0][0] = "Rayquaza";
        classroom[0][1] = "Kyogre";
        classroom[0][2] = "Groudon";

        //second row
        classroom[1][0] = "Sora";
        classroom[1][1] = "Goofy";
        classroom[1][2] = "Donald";

        //third row
        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";

//        System.out.println(Arrays.toString(classroom));

        //nested for loop

/*
        for(int row = 0; row < 3; row++){
            for(int col = 0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }
*/

        //enhanced for loop
        //In Java, a for-each loop can be used to iterate over the items of an array and arrayList.
        //for-each in java for array and arrayList is also called enhanced for loop.

/*

        //member is a parameter which will represent  each item in the given array
        for(String member: members){
            System.out.println(member);
        }
*/
        String[] members = {"Eugene", "Vincent", "Dennis", "Alfred"};
/*        for(String[] row:classroom){
            System.out.println(Arrays.toString(row));
            for(String student:row){
                System.out.println(student);
            }
        }
 */
        //hashmap forEach
        /*

        HashMap has a method for iterating each field-value pair.
        The hashmap for each() requires a lambda expression as its argument.
        The Lambda expression in Java, is a short block of code which takes in parameters and returns a value. Lambda expressiouns are similar to methods but they do not need a name.


        */
/*
        HashMap<String, String> techniques = new HashMap<>();
        techniques.put(members[0], "Spirit Gun");
        techniques.put(members[1], "Black Dragon");
        techniques.put(members[2], "Rose Whip");
        techniques.put(members[3], "spirit Sword");

        System.out.println(techniques);

        techniques.forEach((key, value)-> {
            System.out.println("Member " + key + " uses " + value + ".");
        });
*/

        //Exception Handling
        /*As the scanner methods have specific dataTypes associated with them (nextLine(),nextIn(), nextDouble()), it is best to assume that there will be issues in user input especially if they fail to provide correct datatypes.
         */
        System.out.println("Enter an Integer: ");
        int num = 0;

        // try-catch-finally
        try{
            num = appScanner.nextInt();
        }
        catch(Exception e){
            System.out.println("Invalid Input");
            e.printStackTrace();
        }
        System.out.println(num);


    }
}